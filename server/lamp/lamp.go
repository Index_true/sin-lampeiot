package lamp

import (
	"errors"        //Paquet pour gerer les erreurs
	"fmt"           //Paquet permetant de gerer le formatage des entrées et sorties
	"html/template" //Gestion des templates HTML
	"net/http"      //Paquet permetant de gerer le protocole HTTP
	"path"          //Permet de gerer les urls
	"regexp"        //Permet l'utilisation d'expressions régulières
)

var ErrorFailedToParseColor error = errors.New("Error Failed to parse color") // Création d'une variable de type error pour gérer les erreurs

type lamp struct { //Type de donnée pour une lampe, contient deux infos
	template               *template.Template
	rootURL, apiURL, color string
}

func New(color string, rootURL string, templatePath string) (error, *lamp) {
	/*
		Cette fonction permet de créer une instance la la struct lamp de manière "sécurisée"
	*/
	l := &lamp{color: "#FFFFFF"} // Création de la struct avec des valeurs par défaut
	var err error                // Variable contenant les erreurs

	l.template, err = template.ParseFiles(templatePath) //Ouverture du fichier de template, il sera utilisé par la fonction colorPicker pour générer l'interface web

	if err != nil {
		return err, nil //Si l'ouverture de la template a eu un problème, on retourne une erreur et renvoie une struct lamp égale a nil ( null )
	}

	if color != "" { //On aplique la couleur si elle n'est pas égale a nil ( null ) sinon on met la valeur par défaut ( blanc )
		err = l.SetColor(color) // Application de la couleur

		if err != nil { // Verification des erreurs
			return err, nil //En cas d'erreur, on retourne l'erreur et une struct vide
		}
	}

	/*
		Génération des urls
	*/
	l.rootURL = path.Join("/", rootURL)
	l.apiURL = path.Join(l.rootURL, "/color/")

	return err, l

}

func (l *lamp) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	/*
		Cette fonction est appelée à chaque requete http d'une page appartenant à la lamp
	*/
	u := path.Clean(r.RequestURI)

	switch u {
	case l.rootURL:
		l.template.Execute(w, l.color) // Fonction appelée pour la page /

	case l.apiURL:
		l.colorAPI(w, r) // Fonction appelée pour la page /color

	default:
		http.Error(w, r.RequestURI+" Was not found on this server", http.StatusNotFound) // Fonction apellée pour tout autre page

	}
	return
}

func (l *lamp) SetColor(c string) error {
	/*
		Cette fonction gère la mise à jour de la couleur de la lampe
	*/
	matched, err := regexp.MatchString("(^#[A-Za-z0-9]{3}$)|(^#[A-Za-z0-9]{6}$)", c) // On verifie que le texte entré corresponds bien au format de couleur hexadecimal ( #RRGGBB ou #RGB )

	if err != nil { // Gestion des erreurs
		return err // Si il y a eu une erreur, stopper la fonction et renvoyer la valeur de l'erreur
	}

	if matched {
		l.color = c // Si la couleur passée a la fonction est valide, on met la couleur à jour
	} else {
		return ErrorFailedToParseColor // Dans le cas contraire on indique que le texte passé a la fonction n'est pas une couleur hexadecimale valide
	}
	return nil // Si tout c'est bien passé, on ne retourne aucune erreur

}

func (l *lamp) colorAPI(w http.ResponseWriter, r *http.Request) {
	/*
		Cette fonction gère la page /color,
			Si le client http accède à cette page via une requète GET, on lui renvoie la valeur de la couleur
			Dans le cas d'une requète POST, on remplace la valeur de la couleur en mémoire par celle envoyée par le client dans le body de sa requète avec pour nome color

	*/
	switch r.Method {
	case http.MethodGet: // Si la methode utilisée est GET, on envoie la couleur au client
		fmt.Fprintf(w, l.color)

	case http.MethodPost: // Si la methode utilisée est POST, on vérifie les données envoyées par le client et si c'est une couleur valide on l'enregistre
		err := r.ParseForm() // Lit les données situées dans le body de la requête POST
		if err != nil {
			http.Error(w, "Unable to parse form values", http.StatusBadRequest) //Renvoie une erreur 400 ( Bad request ) si il la lecture du body à rencontré une erreur
		}

		err = l.SetColor(r.FormValue("color")) // Mise à jour de la couleur, la variable err contiendra les erreurs potentielles

		if err == nil { // Gestion des erreurs
			http.Error(w, "", http.StatusNoContent) // Si il n'y a aucune erreur, le code 204 ( No content ) est retourné
		} else if err == ErrorFailedToParseColor {
			http.Error(w, "Failed to parse color", http.StatusBadRequest) // Si le client n'a pas envoyé une couleur valide, le code 400 ( Bad Request ) est retourné
		} else {
			http.Error(w, "Internal server error", http.StatusInternalServerError) // Pour tout autre erreur, le code 500 ( Internal server error ) est retourné
		}
	default:
		http.Error(w, "Bad request", http.StatusBadRequest) // Pour tout autre type de requète, l'erreur 400 ( Bad request ) est renvoyée

	}

}
