package main

import (
	"./lamp" //Paquet créé pour ce projet, il permet de gerer les lampes

	"log"      //Paquet permetant de gerer les logs
	"net/http" //Paquet permetant de gerer le protocole HTTP
)

const colorPickerTemplate = "./ColorPicker.tmpl" //Chemin vers le fichier de template de l'interface web

func main() {
	log.Println("Starting web server:")
	err, lamp := lamp.New("", "/lamp/1", colorPickerTemplate) // Création d'une instance d'un panneau de configuration d'une lampe ( 1 par lampe )
	if err != nil {
		log.Fatal(err) //Si la création de la lampe a retourné une erreur, le programme s'arrête
	}

	http.Handle("/lamp/1/", lamp)                //On indique au serveur web que la struct lamp ( qui implémente l'interface http.Handler ) va gérer toutes les pages sous /lamp/1/ en incluant /lamp/1/
	log.Fatal(http.ListenAndServe(":8080", nil)) //Demarage du serveur web

}
