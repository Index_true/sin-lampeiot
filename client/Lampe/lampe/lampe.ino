/*
   Lampe connectée, récupère une couleur depuis un serveur web et l'affiche sur une lampe
*/
#include <string.h>

#include <Bridge.h> // Librairies pour arduino YUN
#include <HttpClient.h> // Librairies pour le client HTTP

#include <ChainableLED.h> //Librairie pour la led rgb
#define NUM_LEDS 1 //Nombre de led rgb

ChainableLED leds(7, 8, NUM_LEDS);//Variable de la led rgb ( Sur les Pin 7 et 8 )


const String SERVER_URL = "http://172.18.128.160:8080"; // Adresse du serveur web
const String COLOR_URL = "/lamp/1/color"; //Url de la couleur


void setup() {
  /*
     Démarage du pont vers la partie linux de l'arduino YUN, pendant ce démarage, on éteint la led car cela peut prendre plusieurs secondes ( la led éteinte indique que la YUN travaille )
  */
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  Bridge.begin();
  digitalWrite(13, HIGH);
}

void loop() {
  // Démarage du client http
  HttpClient client;
  byte red, green, blue;
  String data;

  
  //Requète vers le serveur web
  client.get(SERVER_URL + COLOR_URL);

  if (client.available() > 0 ) {
    data = client.readString();
    splitColor(data, &red, &green, &blue);
  }
  leds.setColorRGB(0, red,green,blue);


  delay(200);
}

void splitColor(String color, byte *red, byte *green, byte *blue) {
  if (color.length() == 4 || color.length() == 7 ) {
    color = color.substring(1, color.length()); //Suppression du # du debut


    if (color.length() == 6) {
      String r = color.substring(0, 2);//Séparation de la valeur en 3 strings
      String g = color.substring(2, 4);
      String b = color.substring(4, 6);

      *red = (byte) strtol(r.c_str(), 0, 16); //Conversion en byte du rouge
      *green = (byte) strtol(g.c_str(), 0, 16); //Conversion en byte du vert
      *blue = (byte) strtol(b.c_str(), 0, 16); //Conversion en byte du bleu


    } else if ( color.length() == 3 ) {
      String r = color.substring(0, 1);//Séparation de la valeur en 3 strings
      String g = color.substring(1, 2);
      String b = color.substring(2, 3);

      *red = (byte) strtol(r.c_str(), NULL, 16); //Conversion en byte du rouge
      *green = (byte) strtol(g.c_str(), NULL, 16); //Conversion en byte du vert
      *blue = (byte) strtol(b.c_str(), NULL, 16); //Conversion en byte du bleu

    }

  } else { // La valeur ne corresponds pas a une couleur hexadecimale
    *red = 0;
    *green = 0;
    *blue = 0;
    return;
  }
  return;
}
