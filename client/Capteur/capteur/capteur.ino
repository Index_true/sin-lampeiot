
/* HTTP Client librairies */
#include <HttpClient.h>
#include <Bridge.h>

/* Librairies pour le capteur de couleur*/
#include <Wire.h>
#include <GroveColorSensor.h>



#define BUTTON_PIN 4 //Le bouton est sur le pin D4
#define URL "http://172.18.128.160:8080/lamp/1/color"


void setup() {
  /*
     Fonction d'initialisation, elle est lancée une fois lors du démarage du programme
  */
  Wire.begin();//Démarage de la connexion au capteur de couleur

  Bridge.begin();// Démarage de la connexion bridge

  pinMode(BUTTON_PIN, INPUT);// On met le Pin pour le bouton en input

}

void loop() {
  /*
     On est obligés de définir nos variables GroveColorSensor dans cette fonction pour qu'elles soient acessibles
  */
  int red, green, blue; // Ces variables contienderont les couleurs récupérées par le capteur
  GroveColorSensor colorSensor ; // Variable contenant le capteur de couleurs
  colorSensor.ledStatus = 1; // On démare la led du capteur de couleur

  /*Variables pour l'envoi des données*/
  String url = URL; // URL à laquelle envoyer les données
  String data; //Contiendra les données POST a envoyer au serveur



  while (1) {
    /*
      Boucle principale, cette fonction est executée en boucle
    */
    while (!digitalRead(BUTTON_PIN)); // On bloque l'execution du code tant que le bontton n'a pas été appuyé

    /* Récupération de la couleur*/
    colorSensor.readRGB(&red, &green, &blue); // Lecture des couleurs
    delay(300); // On laisse le temps au capteur de récuperer la donnée
    data = generatePostData(red, green, blue); // On génère les données POST à envoyer

    /*Envoi de la couleur*/
    HttpClient h; // Variable contenant le client HTTP
    h.postAsynchronously(url, data); //Envoi des données

    delay(1000); // Evite que la récupération de la couleur se relance immédiatement si l'utilisateur maintient le bouton

  }
}

String generatePostData(int r, int  g, int b) {
  /*
     Cette fonction génère les données a envoyer au serveur ( normalement de la forme
  */
  String data = "";
  String red = String( r, HEX ); //Conversion des différentes couleurs en String hexadecimal
  String green = String( g, HEX );
  String blue = String( b, HEX );


  /* Vérification du rouge*/
  if (red.length() == 1) { // On formate le rouge pour qu'il soit long de deux caractères
    red = "0" + red; //On ajoute un 0 devant le nombre si il ne fait q'un caractère ( ex: "5" --> "05" )
  } else if ( red.length() != 2) {
    red = "00"; //Dans le cas ou le nombre est plus grand que 255, on le met a "00"
  }
  data += red; // On ajoute le rouge aux données a envoyer


  /* Vérification du vert*/
  if ( green.length() == 1) { //Même que pour le rouge, cvoir au dessus
    green = "0" + green;
  } else if ( green.length() != 2) {
    green = "00";
  }
  data += green;


  /* Vérification du bleu*/
  if ( blue.length() == 1 ) { //Même que pour le rouge, cvoir au dessus
    blue = "0" + blue;
  } else if ( blue.length() != 2) {
    blue = "00";
  }
  data += blue;


  data.toUpperCase(); //On met tout les caractère en majuscule ex: "5dE324" --> "5DE324"
  data = "color=#" + data; //Ajout de "color=#" au début

  return data; // On renvoie la donnée formattée
}
